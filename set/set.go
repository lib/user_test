package set

import (
	"github.com/google/uuid"
)

// набор
type Set struct {
	Description *string
	Priority    *int32
	Uuid        uuid.UUID
}

func New() *Set {
	return &Set{}
}

func (u *Set) New() any {
	return New()
}

func (u *Set) SetDescription(in string) {
	u.Description = &in
}

func (u *Set) SetPriority(in int32) {
	u.Priority = &in
}
