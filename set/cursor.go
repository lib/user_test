package set

func Cursor(in []byte) *Set {
	u := New()
	if len(in) != 16 {
		return u
	}
	copy(u.Uuid[:], in)
	return u
}

func (u *Set) Cursor() []byte {
	if u.Uuid != [16]byte{} {
		return u.Uuid[:]
	}
	return nil
}
