package set

import (
	"strconv"
	"strings"

	"codeberg.org/lib/repo"
)

var _ repo.Cond = &Set{}

const (
	DbNamespace  = "policy"
	DbName       = "sets"
	PKUuid       = "id"
	FDescription = "description"
)

func (u Set) Cols(primary bool, queryType repo.QType) []string {
	ret := make([]string, 0, 2)
	ret = append(ret,
		string(FDescription),
	)
	if primary {
		ret = append(ret, string(PKUuid))
	}
	return ret
}
func (u Set) Primaries() []string {
	return []string{string(PKUuid)}
}
func (u Set) Db() string {
	return strings.Join(
		[]string{
			strconv.Quote(string(DbNamespace)),
			strconv.Quote(string(DbName)),
		},
		".")
}

func (u *Set) Equal(in any) bool {
	if u == nil {
		return false
	}
	v, ok := in.(*Set)
	if ok {
		return v.Uuid == u.Uuid
	}
	return ok
}

func (u *Set) FieldByName(in string) any {
	if u != nil {
		switch in {
		case PKUuid:
			return &u.Uuid
		case FDescription:
			return &u.Description
		}
	}
	return nil
}

func (u *Set) ValueByName(in string) any {
	if u != nil {
		switch in {
		case PKUuid:
			if u.Uuid != [16]byte{} {
				return u.Uuid
			}
		case FDescription:
			if u.Description != nil {
				return *u.Description
			}
		}
	}
	return nil
}
