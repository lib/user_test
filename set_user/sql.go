package set_user

import (
	"strconv"
	"strings"

	"codeberg.org/lib/repo"
)

var _ repo.Cond = &SetUser{}

const (
	DbNamespace = "policy"
	DbName      = "users_sets"
	PKUser      = "user"
	PKSet       = "set"
)

func (u SetUser) IsSet() bool {
	return u.UserUuid != [16]byte{} || u.SetUuid != [16]byte{}
}

func (u SetUser) Cols(primary bool, queryType repo.QType) []string {
	ret := make([]string, 0, 2)
	if primary {
		ret = append(ret, string(PKUser), string(PKSet))
	}
	return ret
}
func (u SetUser) Primaries() []string {
	return []string{string(PKUser), string(PKSet)}
}
func (u SetUser) Db() string {
	return strings.Join(
		[]string{
			strconv.Quote(string(DbNamespace)),
			strconv.Quote(string(DbName)),
		},
		".")
}

func (u *SetUser) Equal(in any) bool {
	if u == nil {
		return false
	}
	v, ok := in.(*SetUser)
	if ok {
		return u.UserUuid == v.UserUuid && u.SetUuid == v.SetUuid
	}
	return ok
}

func (u *SetUser) FieldByName(in string) any {
	if u != nil {
		switch in {
		case PKUser:
			return &u.UserUuid
		case PKSet:
			return &u.SetUuid
		}
	}
	return nil
}

func (u *SetUser) ValueByName(in string) any {
	if u != nil {
		switch in {
		case PKUser:
			if u.UserUuid != [16]byte{} {
				return u.UserUuid
			}
		case PKSet:
			if u.SetUuid != [16]byte{} {
				return u.SetUuid
			}
		}
	}
	return nil
}
