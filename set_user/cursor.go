package set_user

func Cursor(in []byte) *SetUser {
	u := New()
	if len(in) != 16*2 {
		return u
	}
	copy(u.SetUuid[:], in[:16])
	copy(u.UserUuid[:], in[16:])
	return u
}

func (u *SetUser) Cursor() []byte {
	if u != nil && u.UserUuid != [16]byte{} && u.SetUuid != [16]byte{} {
		b := make([]byte, 16*2)
		copy(b[:16], u.SetUuid[:])
		copy(b[16:], u.UserUuid[:])
		return b
	}
	return nil
}
