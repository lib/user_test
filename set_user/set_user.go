package set_user

import (
	"sync"

	"github.com/google/uuid"
)

// связь политики <-> пользователь
type SetUser struct {
	SetUuid  uuid.UUID
	UserUuid uuid.UUID
}

var poll = sync.Pool{New: func() interface{} { return new(SetUser) }}

func New() *SetUser {
	return poll.Get().(*SetUser)
}

func (u *SetUser) Reset() {
	u.SetUuid = [16]byte{}
	u.UserUuid = [16]byte{}
}

func (u *SetUser) New() any {
	return New()
}

func (u *SetUser) Free() {
	u.Reset()
	poll.Put(u)
}
