package set_user_test

import (
	"context"
	"database/sql"
	"os"
	"reflect"
	"testing"

	entSet "codeberg.org/lib/user_test/set"
	entUserSet "codeberg.org/lib/user_test/set_user"
	entUser "codeberg.org/lib/user_test/user"

	"codeberg.org/lib/repo"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/uuid"
)

const (
	connInfoTest = `host=10.10.10.1 port=5432 dbname=test user=web password=web sslmode=disable`
)

var conn *sql.DB

func TestMain(m *testing.M) {
	var err error
	conn, err = sql.Open("postgres", connInfoTest)
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = conn.Close()
	}()
	os.Exit(m.Run())
}

func TestSetUserSimple(t *testing.T) {
	ctx := context.Background()
	_, err := conn.Query(`DELETE FROM policy.users_sets CASCADE`)
	if err != nil {
		panic(err)
	}
	_, err = conn.Query(`DELETE FROM policy.sets CASCADE`)
	if err != nil {
		panic(err)
	}
	_, err = conn.Query(`DELETE FROM main.users CASCADE`)
	if err != nil {
		panic(err)
	}
	set := entSet.New()
	set.Uuid = uuid.New()
	set.SetDescription("dasddsadsa")
	err = repo.Insert(ctx, conn, set)
	if err != nil {
		t.Fatal(err)
	}
	usr := entUser.New()
	usr.Uuid = uuid.New()
	usr.Email = "wqeqwe@sdsad.we"
	usr.Password = []byte{1, 2, 3, 4, 5, 6}
	if err = repo.Insert(ctx, conn, usr); err != nil {
		t.Fatal(err)
	}
	obj := entUserSet.New()
	obj.SetUuid = set.Uuid
	obj.UserUuid = usr.Uuid
	err = repo.Insert(ctx, conn, obj)
	if err != nil {
		t.Fatal(err)
	}
	rez := entUserSet.New()
	rez.SetUuid = obj.SetUuid
	rez.UserUuid = obj.UserUuid
	err = repo.Get(ctx, conn, rez)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(obj, rez) {
		spew.Dump(obj, rez)
		t.Fatal("not eq")
	}
	err = repo.Update(ctx, conn, obj)
	if err == repo.ErrNoRowAffected {
		err = nil
	}
	if err != nil {
		t.Fatal(err)
	}
	rez = entUserSet.New()
	rez.SetUuid = obj.SetUuid
	rez.UserUuid = obj.UserUuid
	err = repo.Get(ctx, conn, rez)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(obj, rez) {
		spew.Dump(obj, rez)
		t.Fatal("not eq")
	}
	rez = entUserSet.New()
	rez.SetUuid = obj.SetUuid
	rez.UserUuid = obj.UserUuid
	err = repo.Delete(ctx, conn, rez)
	if err != nil {
		t.Fatal(err)
	}
	err = repo.Get(ctx, conn, rez)
	if err != repo.ErrNoRows {
		t.Fatal(err)
	}
}

func TestComplex(t *testing.T) {
	ctx := context.Background()
	_, err := conn.Query(`DELETE FROM policy.users_sets CASCADE`)
	if err != nil {
		panic(err)
	}
	_, err = conn.Query(`DELETE FROM policy.sets CASCADE`)
	if err != nil {
		panic(err)
	}
	_, err = conn.Query(`DELETE FROM main.users CASCADE`)
	if err != nil {
		panic(err)
	}
	usr := entUser.New()
	usr.Uuid = uuid.New()
	usr.Email = "wqeqwewq@sds.ww"
	usr.Password = []byte{'q', 'w', 'e', 'r', 't', 'y'}
	if err = repo.Insert(ctx, conn, usr); err != nil {
		t.Fatal(err)
	}
	set := entSet.New()
	obj := entUserSet.New()
	const max = 10
	const onPage = 2
	const order = repo.DESC
	var cursor []byte
	for i := 0; i < max; i++ {
		set.Uuid = uuid.New()
		set.SetDescription("dsadsad")
		err = repo.Insert(ctx, conn, set)
		if err != nil {
			t.Fatal(err)
		}
		obj.SetUuid = set.Uuid
		obj.UserUuid = usr.Uuid
		err = repo.Insert(ctx, conn, obj)
		if err != nil {
			t.Fatal(err)
		}
		if i == max/2 {
			cursor = obj.Cursor()
		}
	}
	num := 0
	err = conn.QueryRowContext(ctx, `SELECT count(*) from policy.users_sets`).Scan(&num)
	if err != nil {
		t.Fatal(err)
	}
	if num != max {
		t.Log(num)
		t.Fatal("num err")
	}
	// find user in list
	rez, err := repo.List(ctx, conn,
		entUserSet.Cursor(cursor),
		repo.NewRequest(1, order, false, "", ""),
		nil,
	)
	if err != nil {
		t.Fatal(err)
	}
	if rez.After() != true {
		spew.Dump(rez)
		t.Fatal("after")
	} else if len(rez.Stop()) == 0 {
		spew.Dump(rez)
		t.Fatal("stop")
	}
	if rez.Before() != true {
		spew.Dump(rez)
		t.Fatal("before")
	} else if len(rez.Start()) == 0 {
		spew.Dump(rez)
		t.Fatal("start")
	}
	if len(rez.Get()) != 1 {
		spew.Dump(rez)
		t.Fatal("len")
	}
	tPage := float64(num) / float64(onPage)
	// paging forward by 5
	fu := func(cursor []byte, order repo.Direction, before bool) (out repo.ListRez[*entUserSet.SetUser], err error) {
		return repo.List(ctx, conn,
			entUserSet.Cursor(cursor),
			repo.NewRequest(onPage, order, before, "", ""),
			[]string{entUserSet.PKUser, entUserSet.PKSet},
		)
	}
	// order define
	cursor = []byte{}
	cursorB := []byte{}
	page := 0
	// t.Log("TOTAL rec ", num, "record on page:", onPage, " expect page: ", tPage)
	// t.Log("FORWARD")
	num = 0
	for {
		rez, err = fu(cursor, order, false)
		if err != nil {
			t.Fatal(err)
		}
		page++
		num += len(rez.Get())
		//t.Log("PAGE:", page, "on page:", len(rez.Get()))
		for _, v := range rez.Get() {
			//t.Log(v)
			cursorB = v.Cursor()
		}
		if rez.After() {
			cursor = rez.Stop()
			obj = entUserSet.Cursor(cursor)
			err = repo.Get(ctx, conn, obj)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(obj, rez.Get()[len(rez.Get())-1]) {
				spew.Dump(obj, rez.Get()[len(rez.Get())-1])
				t.Fatal("not eq")
			}
		} else {
			// save last row
			break
		}
	}
	if float64(page) != tPage {
		t.Log(page)
		t.Fatal("page err")
	}
	if num != max {
		t.Log(num)
		t.Fatal("num err")
	}
	// t.Log("BACKWARD")
	// t.Log("BEGIN CURSOR:", Cursor(cursorB))
	// begin from -1
	num--
	for {
		rez, err = fu(cursorB, order, true)
		if err != nil {
			t.Fatal(err)
		}
		//t.Log("PAGE:", page, "on page:", len(rez.Get()))
		for _, v := range rez.Get() {
			_ = v
			//t.Log(v)
		}
		page--
		num -= len(rez.Get())
		if rez.Before() {
			cursorB = rez.Start()
			obj = entUserSet.Cursor(cursorB)
			err = repo.Get(ctx, conn, obj)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(obj, rez.Get()[0]) {
				spew.Dump(obj, rez.Get()[0])
				t.Fatal("not eq")
			}
		} else {
			break
		}
	}
	if page != 0 {
		t.Log(page)
		t.Fatal("page err")
	}
	if num != 0 {
		t.Log(num)
		t.Fatal("num err")
	}
}
