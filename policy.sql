CREATE SCHEMA IF NOT EXISTS policy;
set search_path TO policy;
DROP TABLE IF EXISTS rules CASCADE;
CREATE TABLE IF NOT EXISTS rules
(
    "id"   uuid  not null primary key,
    "description"   varchar(1024) not null,
    "priority"  int default 0,
    "auto" int default 0,
    "conditions"    jsonb
);
DROP TABLE IF EXISTS ones CASCADE;
CREATE TABLE IF NOT EXISTS ones
(
    "id"    uuid    not null primary key,    
    "description"   varchar(1024) not null,
    "algorithm"    int default 0,
    "auto" int default 0,
    "priority"  int default 0
);
DROP TABLE IF EXISTS sets CASCADE;
CREATE TABLE IF NOT EXISTS sets
(
    "id"    uuid    not null primary key,    
    "description"   varchar(1024) not null,
    "auto" int default 0,
    "algorithm"    int default 0
);
DROP TABLE IF EXISTS rules_ones CASCADE;
CREATE TABLE IF NOT EXISTS rules_ones
(
    "rule" uuid    not null REFERENCES rules ("id") ON UPDATE CASCADE ON DELETE CASCADE,
    "one"  uuid    not null REFERENCES ones ("id") ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY ("rule","one")
);
DROP TABLE IF EXISTS sets_ones CASCADE;
CREATE TABLE IF NOT EXISTS sets_ones
(
    "set"  uuid    not null REFERENCES sets ("id") ON UPDATE CASCADE ON DELETE CASCADE,
    "one"  uuid    not null REFERENCES ones ("id") ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY ("one", "set")
);
DROP TABLE IF EXISTS users_sets CASCADE;
CREATE TABLE IF NOT EXISTS users_sets
(
    "user"  uuid    not null REFERENCES "main".users ("uuid") ON UPDATE CASCADE ON DELETE CASCADE,
    "set"   uuid    not null REFERENCES sets ("id") ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY ("user","set")
);
