package rule_one

func Cursor(in []byte) *RuleOne {
	u := New()
	if len(in) != 16*2 {
		return u
	}
	copy(u.RuleUuid[:], in[:16])
	copy(u.OneUuid[:], in[16:])

	return u
}

func (u *RuleOne) Cursor() []byte {
	if u != nil && u.OneUuid != [16]byte{} && u.RuleUuid != [16]byte{} {
		b := make([]byte, 16*2)
		copy(b[:16], u.RuleUuid[:])
		copy(b[16:], u.OneUuid[:])
		return b
	}
	return nil
}
