package rule_one

import (
	"sync"

	"github.com/google/uuid"
)

// связь правило <-> политика
type RuleOne struct {
	RuleUuid uuid.UUID
	OneUuid  uuid.UUID
}

var poll = sync.Pool{New: func() interface{} { return new(RuleOne) }}

func New() *RuleOne {
	return poll.Get().(*RuleOne)
}

func (u *RuleOne) Reset() {
	u.RuleUuid = [16]byte{}
	u.OneUuid = [16]byte{}
}

func (u *RuleOne) New() any {
	return New()
}

func (u *RuleOne) Free() {
	u.Reset()
	poll.Put(u)
}
