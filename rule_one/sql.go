package rule_one

import (
	"strconv"
	"strings"

	"codeberg.org/lib/repo"
)

var _ repo.Cond = &RuleOne{}

const (
	DbNamespace = "policy"
	DbName      = "rules_ones"
	PKRule      = "rule"
	PKOne       = "one"
)

func (u RuleOne) IsSet() bool {
	return u.RuleUuid != [16]byte{} || u.OneUuid != [16]byte{}
}

func (u RuleOne) Cols(primary bool, queryType repo.QType) []string {
	ret := make([]string, 0, 2)
	if primary {
		ret = append(ret, string(PKRule), string(PKOne))
	}
	return ret
}
func (u RuleOne) Primaries() []string {
	return []string{string(PKRule), string(PKOne)}
}
func (u RuleOne) Db() string {
	return strings.Join(
		[]string{
			strconv.Quote(string(DbNamespace)),
			strconv.Quote(string(DbName)),
		},
		".")
}

func (u *RuleOne) Equal(in any) bool {
	if u == nil {
		return false
	}
	v, ok := in.(*RuleOne)
	if ok {
		return u.OneUuid == v.OneUuid && u.RuleUuid == v.RuleUuid
	}
	return ok
}

func (u *RuleOne) FieldByName(in string) any {
	if u != nil {
		switch in {
		case PKRule:
			return &u.RuleUuid
		case PKOne:
			return &u.OneUuid
		}
	}
	return nil
}

func (u *RuleOne) ValueByName(in string) any {
	if u != nil {
		switch in {
		case PKRule:
			if u.RuleUuid != [16]byte{} {
				return u.RuleUuid
			}
		case PKOne:
			if u.OneUuid != [16]byte{} {
				return u.OneUuid
			}
		}
	}
	return nil
}
