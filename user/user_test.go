package user_test

import (
	"context"
	"database/sql"
	"os"
	"reflect"
	"testing"

	"codeberg.org/lib/repo"
	entUser "codeberg.org/lib/user_test/user"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/uuid"
)

const (
	connInfoTest = `host=10.10.10.1 port=5432 dbname=test user=web password=web sslmode=disable`
)

var conn *sql.DB

func TestMain(m *testing.M) {
	var err error
	conn, err = sql.Open("postgres", connInfoTest)
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = conn.Close()
	}()
	os.Exit(m.Run())
}

func TestSimple(t *testing.T) {
	_, err := conn.Query(`DELETE FROM main.users CASCADE`)
	if err != nil {
		panic(err)
	}
	ctx := context.Background()
	obj := entUser.New()
	if err = repo.Get(ctx, conn, obj); err != repo.ErrNoRows {
		t.Fatal(err)
	}
	obj.Uuid = uuid.New()
	obj.Email = "11@11.22"
	obj.Password = []byte{1, 2, 3, 4, 5}
	if err = repo.Insert(ctx, conn, obj); err != nil {
		t.Fatal(err)
	}
	rez := entUser.New()
	rez.Uuid = obj.Uuid
	if err = repo.Get(ctx, conn, rez); err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(obj, rez) {
		t.Fatal("not eq")
	}
	obj.Email = "22@11.22"
	obj.Password = []byte{4, 2, 3, 4, 5}
	if err = repo.Update(ctx, conn, obj); err != nil {
		t.Fatal(err)
	}
	rez = entUser.New()
	rez.Uuid = obj.Uuid
	if err = repo.Get(ctx, conn, rez); err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(obj, rez) {
		t.Fatal("not eq")
	}
}

func TestComplex(t *testing.T) {
	ctx := context.Background()
	_, err := conn.Query(`DELETE FROM main.users CASCADE`)
	if err != nil {
		t.Fatal(err)
	}
	u := entUser.New()
	const max = 10
	const onPage = 2
	const order = repo.DESC
	var cursor []byte
	for i := 0; i < max; i++ {
		u.Uuid = uuid.New()
		u.Email = string(byte(i+'A')) + "qq@" + "11,22"
		u.Password = []byte{1, 2, 3, 4, 5}
		err = repo.Insert(ctx, conn, u)
		if err != nil {
			t.Fatal(err)
		}
		if i == max/2 {
			cursor = u.Cursor()
		}
	}
	num := 0
	err = conn.QueryRowContext(ctx, `SELECT count(*) from main.users`).Scan(&num)
	if err != nil {
		t.Fatal(err)
	}
	if num != max {
		t.Log(num)
		t.Fatal("num err")
	}
	// find user in list
	rez, err := repo.List(ctx, conn,
		entUser.Cursor(cursor),
		repo.NewRequest(1, order, false, "", ""),
		nil,
	)
	if err != nil {
		t.Fatal(err)
	}
	if rez.After() != true {
		spew.Dump(rez)
		t.Fatal("after")
	} else if len(rez.Stop()) == 0 {
		spew.Dump(rez)
		t.Fatal("stop")
	}
	if rez.Before() != true {
		spew.Dump(rez)
		t.Fatal("before")
	} else if len(rez.Start()) == 0 {
		spew.Dump(rez)
		t.Fatal("start")
	}
	if len(rez.Get()) != 1 {
		spew.Dump(rez)
		t.Fatal("len")
	}

	tPage := float64(num) / float64(onPage)
	// paging forward by 5
	fu := func(cursor []byte, order repo.Direction, before bool) (out repo.ListRez[*entUser.User], err error) {
		return repo.List(ctx, conn,
			entUser.Cursor(cursor),
			repo.NewRequest(onPage, order, before, "", ""),
			[]string{entUser.UKEmail, entUser.PKUuid},
		)
	}
	// order define
	cursor = []byte{}
	cursorB := []byte{}
	page := 0
	t.Log("TOTAL rec ", num, "record on page:", onPage, " expect page: ", tPage)
	t.Log("FORWARD")
	num = 0
	for {
		rez, err = fu(cursor, order, false)
		if err != nil {
			t.Fatal(err)
		}
		page++
		num += len(rez.Get())
		t.Log("PAGE:", page, "on page:", len(rez.Get()))
		for _, v := range rez.Get() {
			t.Log(v)
			cursorB = v.Cursor()
		}
		if rez.After() {
			cursor = rez.Stop()
			u = entUser.Cursor(cursor)
			err = repo.Get(ctx, conn, u)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(u, rez.Get()[len(rez.Get())-1]) {
				spew.Dump(u, rez.Get()[len(rez.Get())-1])
				t.Fatal("not eq")
			}
		} else {
			// save last row
			break
		}
	}
	if float64(page) != tPage {
		t.Log(page)
		t.Fatal("page err")
	}
	if num != max {
		t.Log(num)
		t.Fatal("num err")
	}
	t.Log("BACKWARD")
	t.Log("BEGIN CURSOR:", entUser.Cursor(cursorB))
	// begin from -1
	num--
	for {
		rez, err = fu(cursorB, order, true)
		if err != nil {
			t.Fatal(err)
		}
		t.Log("PAGE:", page, "on page:", len(rez.Get()))
		for _, v := range rez.Get() {
			//_ = v
			t.Log(v)
		}
		page--
		num -= len(rez.Get())
		if rez.Before() {
			cursorB = rez.Start()
			u = entUser.Cursor(cursorB)
			err = repo.Get(ctx, conn, u)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(u, rez.Get()[0]) {
				spew.Dump(u, rez.Get()[0])
				t.Fatal("not eq")
			}
		} else {
			break
		}
	}
	if page != 0 {
		t.Log(page)
		t.Fatal("page err")
	}
	if num != 0 {
		t.Log(num)
		t.Fatal("num err")
	}
}
