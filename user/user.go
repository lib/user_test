package user

import (
	"github.com/google/uuid"
)

type User struct {
	Email    string
	Password []byte
	Uuid     uuid.UUID
}

func New() *User {
	return &User{}
}

func (u *User) New() any {
	return New()
}
