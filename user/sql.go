package user

import (
	"strconv"
	"strings"

	"codeberg.org/lib/repo"
)

const (
	DbNamespace = "main"
	DbName      = "users"
	PKUuid      = "uuid"
	UKEmail     = "email"
	FPassword   = "password"
)

var _ repo.Cond = &User{}

func (u User) Cols(primary bool, queryType repo.QType) []string {
	ret := make([]string, 0, 3)
	ret = append(ret,
		string(UKEmail),
		string(FPassword),
	)
	if primary {
		ret = append(ret, string(PKUuid))
	}
	return ret
}
func (u User) Primaries() []string {
	return []string{string(PKUuid)}
}

func (u User) Db() string {
	return strings.Join(
		[]string{
			strconv.Quote(string(DbNamespace)),
			strconv.Quote(string(DbName)),
		},
		".")
}

func (u *User) Equal(in any) bool {
	if u == nil {
		return false
	}
	v, ok := in.(*User)
	if ok {
		return v.Uuid == u.Uuid
	}
	return ok
}

func (u *User) FieldByName(in string) any {
	if u != nil {
		switch in {
		case PKUuid:
			return &u.Uuid
		case UKEmail:
			return &u.Email
		case FPassword:
			return &u.Password
		}
	}
	return nil
}

func (u *User) ValueByName(in string) any {
	if u != nil {
		switch in {
		case PKUuid:
			if u.Uuid != [16]byte{} {
				return u.Uuid
			}
		case UKEmail:
			if len(u.Email) > 0 {
				return u.Email
			}
		case FPassword:
			if u.Password != nil {
				return u.Password
			}
		}
	}
	return nil
}
