package user

func Cursor(in []byte) *User {
	u := New()
	if len(in) != 16 {
		return u
	}
	copy(u.Uuid[:], in)
	return u
}

func (u User) Cursor() []byte {
	if u.Uuid != [16]byte{} {
		return u.Uuid[:]
	}
	return nil
}
