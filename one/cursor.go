package one

func Cursor(in []byte) *One {
	u := New()
	if len(in) != 16 {
		return u
	}
	copy(u.Uuid[:], in)
	return u
}

func (u *One) Cursor() []byte {
	if u.Uuid != [16]byte{} {
		return u.Uuid[:]
	}
	return nil
}
