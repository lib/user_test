package one

import (
	"strconv"
	"strings"

	"codeberg.org/lib/repo"
)

var _ repo.Cond = &One{}

const (
	DbNamespace  = "policy"
	DbName       = "ones"
	PKUuid       = "id"
	FDescription = "description"
	FPriority    = "priority"
)

func (u One) Cols(primary bool, queryType repo.QType) []string {
	ret := make([]string, 0, 3)
	ret = append(ret,
		string(FDescription),
		string(FPriority),
	)
	if primary {
		ret = append(ret, string(PKUuid))
	}
	return ret
}
func (u One) Primaries() []string {
	return []string{string(PKUuid)}
}
func (u One) Db() string {
	return strings.Join(
		[]string{
			strconv.Quote(string(DbNamespace)),
			strconv.Quote(string(DbName)),
		},
		".")
}

func (u *One) Equal(in any) bool {
	if u == nil {
		return false
	}
	v, ok := in.(*One)
	if ok {
		return v.Uuid == u.Uuid
	}
	return ok
}

func (u *One) FieldByName(in string) any {
	if u != nil {
		switch in {
		case PKUuid:
			return &u.Uuid
		case FDescription:
			return &u.Description
		case FPriority:
			return &u.Priority
		}
	}
	return nil
}

func (u *One) ValueByName(in string) any {
	if u != nil {
		switch in {
		case PKUuid:
			if u.Uuid != [16]byte{} {
				return u.Uuid
			}
		case FDescription:
			if u.Description != nil {
				return *u.Description
			}
		case FPriority:
			if u.Priority != nil {
				return *u.Priority
			}
		}
	}
	return nil
}
