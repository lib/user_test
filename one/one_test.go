package one_test

import (
	"context"
	"database/sql"
	"math/rand"
	"os"
	"reflect"
	"testing"

	"codeberg.org/lib/repo"
	entOne "codeberg.org/lib/user_test/one"

	"github.com/davecgh/go-spew/spew"
	"github.com/google/uuid"
)

const (
	connInfoTest = `host=10.10.10.1 port=5432 dbname=test user=web password=web sslmode=disable`
)

var conn *sql.DB

func TestMain(m *testing.M) {
	var err error
	conn, err = sql.Open("postgres", connInfoTest)
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = conn.Close()
	}()
	os.Exit(m.Run())
}

func TestOneSimple(t *testing.T) {
	ctx := context.Background()
	_, err := conn.Query(`DELETE FROM policy.ones CASCADE`)
	if err != nil {
		panic(err)
	}
	obj := entOne.New()
	obj.Uuid = uuid.New()
	obj.SetDescription("232131czxcxz")
	obj.SetPriority(rand.Int31())
	err = repo.Insert(ctx, conn, obj)
	if err != nil {
		t.Fatal(err)
	}
	rez := entOne.New()
	rez.Uuid = obj.Uuid
	err = repo.Get(ctx, conn, rez)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(obj, rez) {
		spew.Dump(obj, rez)
		t.Fatal("not eq")
	}
	obj.SetDescription("fsdfsdfsdfsdfsd")
	err = repo.Update(ctx, conn, obj)
	if err == repo.ErrNoRowAffected {
		err = nil
	}
	if err != nil {
		t.Fatal(err)
	}
	rez = entOne.New()
	rez.Uuid = obj.Uuid
	err = repo.Get(ctx, conn, rez)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(obj, rez) {
		spew.Dump(obj, rez)
		t.Fatal("not eq")
	}
	rez = entOne.New()
	rez.Uuid = obj.Uuid
	err = repo.Delete(ctx, conn, rez)
	if err != nil {
		t.Fatal(err)
	}
	err = repo.Get(ctx, conn, rez)
	if err != repo.ErrNoRows {
		t.Fatal(err)
	}
}

func TestComplex(t *testing.T) {
	ctx := context.Background()
	_, err := conn.Query(`DELETE FROM policy.ones CASCADE`)
	if err != nil {
		panic(err)
	}
	obj := entOne.New()
	const max = 10
	const onPage = 2
	const order = repo.DESC
	var cursor []byte
	for i := 0; i < max; i++ {
		obj.Uuid = uuid.New()
		obj.SetDescription(string(byte(i+'A')) + "_" + "sdsdasd")
		obj.SetPriority(rand.Int31())
		err = repo.Insert(ctx, conn, obj)
		if err != nil {
			t.Fatal(err)
		}
		if i == max/2 {
			cursor = obj.Cursor()
		}
	}
	num := 0
	err = conn.QueryRowContext(ctx, `SELECT count(*) from policy.ones`).Scan(&num)
	if err != nil {
		t.Fatal(err)
	}
	if num != max {
		t.Log(num)
		t.Fatal("num err")
	}
	// find user in list
	rez, err := repo.List(ctx, conn,
		entOne.Cursor(cursor),
		repo.NewRequest(1, order, false, "", ""),
		nil,
	)
	if err != nil {
		t.Fatal(err)
	}
	if rez.After() != true {
		spew.Dump(rez)
		t.Fatal("after")
	} else if len(rez.Stop()) == 0 {
		spew.Dump(rez)
		t.Fatal("stop")
	}
	if rez.Before() != true {
		spew.Dump(rez)
		t.Fatal("before")
	} else if len(rez.Start()) == 0 {
		spew.Dump(rez)
		t.Fatal("start")
	}
	if len(rez.Get()) != 1 {
		spew.Dump(rez)
		t.Fatal("len")
	}
	tPage := float64(num) / float64(onPage)
	// paging forward by 5
	fu := func(cursor []byte, order repo.Direction, before bool) (out repo.ListRez[*entOne.One], err error) {
		return repo.List(ctx, conn,
			entOne.Cursor(cursor),
			repo.NewRequest(onPage, order, before, "", ""),
			[]string{entOne.FDescription, entOne.PKUuid},
		)
	}
	// order define
	cursor = []byte{}
	cursorB := []byte{}
	page := 0
	// t.Log("TOTAL rec ", num, "record on page:", onPage, " expect page: ", tPage)
	// t.Log("FORWARD")
	num = 0
	for {
		rez, err = fu(cursor, order, false)
		if err != nil {
			t.Fatal(err)
		}
		page++
		num += len(rez.Get())
		//t.Log("PAGE:", page, "on page:", len(rez.Get()))
		for _, v := range rez.Get() {
			//t.Log(v)
			cursorB = v.Cursor()
		}
		if rez.After() {
			cursor = rez.Stop()
			obj = entOne.Cursor(cursor)
			err = repo.Get(ctx, conn, obj)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(obj, rez.Get()[len(rez.Get())-1]) {
				spew.Dump(obj, rez.Get()[len(rez.Get())-1])
				t.Fatal("not eq")
			}
		} else {
			// save last row
			break
		}
	}
	if float64(page) != tPage {
		t.Log(page)
		t.Fatal("page err")
	}
	if num != max {
		t.Log(num)
		t.Fatal("num err")
	}
	// t.Log("BACKWARD")
	// t.Log("BEGIN CURSOR:", Cursor(cursorB))
	// begin from -1
	num--
	for {
		rez, err = fu(cursorB, order, true)
		if err != nil {
			t.Fatal(err)
		}
		//t.Log("PAGE:", page, "on page:", len(rez.Get()))
		for _, v := range rez.Get() {
			_ = v
			//t.Log(v)
		}
		page--
		num -= len(rez.Get())
		if rez.Before() {
			cursorB = rez.Start()
			obj = entOne.Cursor(cursorB)
			err = repo.Get(ctx, conn, obj)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(obj, rez.Get()[0]) {
				spew.Dump(obj, rez.Get()[0])
				t.Fatal("not eq")
			}
		} else {
			break
		}
	}
	if page != 0 {
		t.Log(page)
		t.Fatal("page err")
	}
	if num != 0 {
		t.Log(num)
		t.Fatal("num err")
	}
}
