package one

import (
	"github.com/google/uuid"
)

type One struct {
	Description *string
	Priority    *int32
	Uuid        uuid.UUID
}

func New() *One {
	return &One{}
}

func (u *One) New() any {
	return New()
}

func (u *One) SetDescription(in string) {
	u.Description = &in
}

func (u *One) SetPriority(in int32) {
	u.Priority = &in
}
