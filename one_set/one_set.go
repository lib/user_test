package one_set

import (
	"sync"

	"github.com/google/uuid"
)

// связь политика <-> политики
type OneSet struct {
	OneUuid uuid.UUID
	SetUuid uuid.UUID
}

var poll = sync.Pool{New: func() interface{} { return new(OneSet) }}

func New() *OneSet {
	return poll.Get().(*OneSet)
}

func (u *OneSet) Reset() {
	u.OneUuid = [16]byte{}
	u.SetUuid = [16]byte{}
}

func (u *OneSet) New() any {
	return New()
}

func (u *OneSet) Free() {
	u.Reset()
	poll.Put(u)
}
