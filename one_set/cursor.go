package one_set

func Cursor(in []byte) *OneSet {
	u := New()
	if len(in) != 16*2 {
		return u
	}
	copy(u.OneUuid[:], in[:16])
	copy(u.SetUuid[:], in[16:])
	return u
}

func (u *OneSet) Cursor() []byte {
	if u != nil && u.OneUuid != [16]byte{} && u.SetUuid != [16]byte{} {
		b := make([]byte, 16*2)
		copy(b[:16], u.OneUuid[:])
		copy(b[16:], u.SetUuid[:])
		return b
	}
	return nil
}
