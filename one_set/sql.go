package one_set

import (
	"strconv"
	"strings"

	"codeberg.org/lib/repo"
)

var _ repo.Cond = &OneSet{}

const (
	DbNamespace = "policy"
	DbName      = "sets_ones"
	PKSet       = "set"
	PKOne       = "one"
)

func (u OneSet) IsSet() bool {
	return u.SetUuid != [16]byte{} || u.OneUuid != [16]byte{}
}

func (u OneSet) Cols(primary bool, queryType repo.QType) []string {
	ret := make([]string, 0, 2)
	if primary {
		ret = append(ret, string(PKSet), string(PKOne))
	}
	return ret
}
func (u OneSet) Primaries() []string {
	return []string{string(PKSet), string(PKOne)}
}
func (u OneSet) Db() string {
	return strings.Join(
		[]string{
			strconv.Quote(string(DbNamespace)),
			strconv.Quote(string(DbName)),
		},
		".")
}

func (u *OneSet) Equal(in any) bool {
	if u == nil {
		return false
	}
	v, ok := in.(*OneSet)
	if ok {
		return u.OneUuid == v.OneUuid && u.SetUuid == v.SetUuid
	}
	return ok
}

func (u *OneSet) FieldByName(in string) any {
	if u != nil {
		switch in {
		case PKSet:
			return &u.SetUuid
		case PKOne:
			return &u.OneUuid
		}
	}
	return nil
}

func (u *OneSet) ValueByName(in string) any {
	if u != nil {
		switch in {
		case PKSet:
			if u.SetUuid != [16]byte{} {
				return u.SetUuid
			}
		case PKOne:
			if u.OneUuid != [16]byte{} {
				return u.OneUuid
			}
		}
	}
	return nil
}
