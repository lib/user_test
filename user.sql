CREATE SCHEMA IF NOT EXISTS main;
SET search_path TO main;
DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE IF NOT EXISTS users
(
    "uuid"       uuid          not null primary key,
    "email"      varchar(1024) not null unique,
    "password"   bytea not null
);
