package condition

// Effect эффект
type Effect int

const (
	// не установленно
	EffectNotSet Effect = iota
	// разрешено
	EffectPermit
	// запрещено
	EffectDeny
)
