package condition

type Expression struct {
	Cond []string
	Effect
}
