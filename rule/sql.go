package rule

import (
	"encoding/json"
	"strconv"
	"strings"

	"codeberg.org/lib/repo"
)

var _ repo.Cond = &Rule{}

const (
	DbNamespace  = "policy"
	DbName       = "rules"
	PKUuid       = "id"
	FDescription = "description"
	FPriority    = "priority"
	FConditions  = "conditions"
)

func (u Rule) Cols(primary bool, queryType repo.QType) []string {
	ret := make([]string, 0, 4)
	ret = append(ret,
		string(FDescription),
		string(FPriority),
		string(FConditions),
	)
	if primary {
		ret = append(ret, string(PKUuid))
	}
	return ret
}
func (u Rule) Primaries() []string {
	return []string{string(PKUuid)}
}
func (u Rule) Db() string {
	return strings.Join(
		[]string{
			strconv.Quote(string(DbNamespace)),
			strconv.Quote(string(DbName)),
		},
		".")
}

func (u *Rule) Equal(in any) bool {
	if u == nil {
		return false
	}
	v, ok := in.(*Rule)
	if ok {
		return v.Uuid == u.Uuid
	}
	return ok
}

func (u *Rule) FieldByName(in string) any {
	if u != nil {
		switch in {
		case PKUuid:
			return &u.Uuid
		case FDescription:
			return &u.Description
		case FPriority:
			return &u.Priority
		case FConditions:
			return &repo.NullJson{
				Handler: func(value []byte, ok bool) {
					if ok && len(value) > 3 {
						if err := json.Unmarshal(value, &u.Conditions); err != nil {
							panic(err)
						}
					} else {
						u.Conditions = nil
					}
				},
			}
		}
	}
	return nil
}

func (u *Rule) ValueByName(in string) any {
	if u != nil {
		switch in {
		case PKUuid:
			if u.Uuid != [16]byte{} {
				return u.Uuid
			}
		case FDescription:
			if u.Description != nil {
				return *u.Description
			}
		case FPriority:
			if u.Priority != nil {
				return *u.Priority
			}
		case FConditions:
			if len(u.Conditions) > 0 {
				b, err := json.Marshal(u.Conditions)
				if err != nil {
					panic(err)
				}
				return b
			}
		}
	}
	return nil
}
