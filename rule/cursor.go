package rule

func Cursor(in []byte) *Rule {
	u := New()
	if len(in) != 16 {
		return u
	}
	copy(u.Uuid[:], in)
	return u
}

func (u *Rule) Cursor() []byte {
	if u.Uuid != [16]byte{} {
		return u.Uuid[:]
	}
	return nil
}
