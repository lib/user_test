package rule

import (
	entRuleCondition "codeberg.org/lib/user_test/rule/condition"

	"github.com/google/uuid"
)

type Rule struct {
	Description *string
	Priority    *int32
	Conditions  []entRuleCondition.Expression
	Uuid        uuid.UUID
}

func New() *Rule {
	return &Rule{}
}

func (u *Rule) New() any {
	return New()
}

func (u *Rule) SetDescription(in string) {
	u.Description = &in
}

func (u *Rule) SetPriority(in int32) {
	u.Priority = &in
}
